package com.rayhanjanam.android.healthpal.associate;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Rayhan on 06-Feb-16.
 */
public class NetworkConnection {
    private Context appContext;

    public NetworkConnection(Context context) {
        this.appContext = context;
    }

    public boolean HasNetworkConnection() {
        boolean hasWifiConnection = false;
        boolean hasCellularConnection = false;

        ConnectivityManager connectivityManager = (ConnectivityManager)
                appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        for (NetworkInfo networkInfo : networkInfos) {
            if (networkInfo.getTypeName().equalsIgnoreCase("WIFI"))
                if (networkInfo.isConnected())
                    hasWifiConnection = true;
            if (networkInfo.getTypeName().equalsIgnoreCase("MOBILE"))
                if (networkInfo.isConnected())
                    hasCellularConnection = true;
        }
        return hasCellularConnection || hasWifiConnection;
    }
}
