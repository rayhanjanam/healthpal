package com.rayhanjanam.android.healthpal;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.rayhanjanam.android.healthpal.gplace.GooglePlace;
import com.rayhanjanam.android.healthpal.gplace.GooglePlaceService;

import java.util.ArrayList;

public class MapsActivity extends AppCompatActivity {

    private GoogleMap mMap; // Might be null if Google Play services APK is not available.
    private Location location;
    private LocationManager locationManager;
    private LocationListener locationListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        //getCurrentLocation();
        setUpMapIfNeeded();
    }

    private void getCurrentLocation() {
        /*
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        ContentResolver contentResolver = getBaseContext()
                .getContentResolver();
        boolean gpsStatus = Settings.Secure
                .isLocationProviderEnabled(contentResolver,
                        LocationManager.GPS_PROVIDER);
        if (gpsStatus) {
                locationListener = new LocationListener() {

                @Override
                public void onLocationChanged(Location location) {
                    MapsActivity.this.location = location;
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED 
                            && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    locationManager.removeUpdates(locationListener);
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            };

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            locationManager.requestLocationUpdates(LocationManager
                    .GPS_PROVIDER, 5000, 10, locationListener);
        } else {
            Toast.makeText(MapsActivity.this, "You need to turn on location services", Toast.LENGTH_SHORT).show();
        }
        */
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        String provider = locationManager
                .getBestProvider(new Criteria(), false);

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        Location loc = locationManager.getLastKnownLocation(provider);

        if (loc == null) {
            locationManager.requestLocationUpdates(provider, 0, 0, listener);
        } else {
            location = loc;
        }
    }

    private LocationListener listener = new LocationListener() {

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }

        @Override
        public void onLocationChanged(Location loc) {
            location = loc;
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            locationManager.removeUpdates(listener);
        }
    };


    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                mMap.getUiSettings().setCompassEnabled(true);
                mMap.getUiSettings().setZoomControlsEnabled(true);
                mMap.getUiSettings().setZoomGesturesEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(10, 10)).title("Marker"));
        mMap.addMarker(new MarkerOptions().position(new LatLng(20, 20)).title("Marker"));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_hospitals:
                setUpHospitals();
                break;
            case R.id.action_donors:
                setUpDonors();
                break;
            case R.id.action_help:
                openHelp();
                break;
            case R.id.action_settings:
                openSettings();
                break;
        }
        return true;
    }

    private void openSettings() {

    }

    private void openHelp() {

    }

    private void setUpDonors() {

    }

    private void setUpHospitals() {
        new GetHospitals(MapsActivity.this).execute();
    }

    public class GetHospitals extends AsyncTask<String, String, ArrayList<GooglePlace>> {
        private ProgressDialog dialog;
        private Context context;
        private ArrayList<GooglePlace> places = new ArrayList<>();

        public GetHospitals(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog = new ProgressDialog(context);
            dialog.setCancelable(false);
            dialog.setMessage("Please wait while loading");
            dialog.setIndeterminate(true);
            dialog.show();
        }

        @Override
        protected ArrayList<GooglePlace> doInBackground(String... params) {
            GooglePlaceService placeService = new GooglePlaceService();
            placeService.setAPI_KEY(getString(R.string.google_place_api_key));

            //ArrayList<GooglePlace> places = placeService.findGooglePlaces(location.getLatitude(),
            //        location.getLongitude(), "Hospitals");

            places = placeService.findGooglePlaces(22.9006342, 89.5013834, "hospital");

            return places;
        }

        @Override
        protected void onPostExecute(ArrayList<GooglePlace> googlePlaces) {
            dialog.dismiss();

            mMap.clear();

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(22.9006342, 89.5013834))
                    .zoom(15)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

            for (int i = 0; i < places.size(); i++) {

                MarkerOptions markerOptions = new MarkerOptions();
                markerOptions.title(places.get(i).getName());

                LatLng marker_pos = new LatLng(places.get(i).getLongitude(),
                        places.get(i).getLatitude());
                markerOptions.position(marker_pos);
                markerOptions.snippet(places.get(i).getVicinity());
                mMap.addMarker(markerOptions);
            }
        }
    }
}

