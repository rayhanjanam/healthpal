package com.rayhanjanam.android.healthpal;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new PreloadData().execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    public class PreloadData extends AsyncTask<String, String, String> {

        private boolean loginStatus;
        private String username, password;

        @Override
        protected void onPreExecute() {
            SharedPreferences sharedPreferences = getSharedPreferences("LOGIN_PREF", MODE_PRIVATE);

            loginStatus = sharedPreferences.getBoolean("key_loginStatus", false);
            username = sharedPreferences.getString("key_username", "");
            password = sharedPreferences.getString("Key_password", "");
        }

        @Override
        protected String doInBackground(String... params) {
            if (loginStatus && hasNetwork())
                executeLogin();
            else {

                Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(loginIntent);
            }

            return null;
        }

        private void executeLogin() {
            Thread timer = new Thread() {

                @Override
                public void run() {
                    try {
                        sleep(750);

                    } catch (InterruptedException e) {
                        Toast.makeText(getBaseContext(),
                                "Error: " + e.getMessage().toString(),
                                Toast.LENGTH_SHORT).show();
                    }

                    // finally if signed in then go to map view
                    // go to sign in view
                    finally {
                        Intent mapIntent = new Intent(
                                getApplicationContext(), MapsActivity.class);
                        startActivity(mapIntent);
                    }
                }

                ;
            };

            timer.start();
        }

        private boolean hasNetwork() {
            boolean haveConnectedWifi = false;
            boolean haveConnectedMobile = false;

            ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
            return haveConnectedWifi || haveConnectedMobile;
        }

    }
}
