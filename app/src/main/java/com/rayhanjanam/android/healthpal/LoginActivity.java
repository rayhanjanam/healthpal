package com.rayhanjanam.android.healthpal;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.rayhanjanam.android.healthpal.associate.JSONParser;
import com.rayhanjanam.android.healthpal.associate.NetworkConnection;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etUsername, etPassword;
    private Button btnLogin;
    private TextView tvSignUp;
    private CheckBox cbLoggedIn;

    private String username, password;
    private boolean isLoginSaving;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        init();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle(R.string.app_name);
        builder.setMessage("Are you sure want to quit?");
        builder.setCancelable(false);

        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                LoginActivity.this.finish();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        AlertDialog quitAlert = builder.create();
        quitAlert.show();
    }

    private void init() {
        etUsername = (EditText) findViewById(R.id.login_etUsername);
        etPassword = (EditText) findViewById(R.id.login_etPassword);
        btnLogin = (Button) findViewById(R.id.login_btnLogin);
        tvSignUp = (TextView) findViewById(R.id.login_tvSignUpNow);
        cbLoggedIn = (CheckBox) findViewById(R.id.login_cbLoggedIn);

        SpannableString content = new SpannableString(getString(R.string.hp_login_tv_signupnw));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        tvSignUp.setText(content);

        btnLogin.setOnClickListener(this);
        tvSignUp.setOnClickListener(this);

        cbLoggedIn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    isLoginSaving = true;
                else
                    isLoginSaving = false;
            }
        });

        SharedPreferences preferences = getSharedPreferences("LOGIN_PREF", 0);
        etUsername.setText(preferences.getString("key_username", ""));
        etPassword.setText(preferences.getString("key_password", ""));

        if (!etUsername.getText().toString().isEmpty() && !etPassword.getText().toString().isEmpty())
            cbLoggedIn.setChecked(true);
        else
            cbLoggedIn.setChecked(false);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.login_btnLogin:
                executeLogin();
                break;
            case R.id.login_tvSignUpNow:
                Intent signupIntent = new Intent(getApplicationContext(), SignupActivity.class);
                startActivity(signupIntent);
                break;
            default:
                break;
        }
    }

    private void executeLogin() {
        NetworkConnection connection = new NetworkConnection(this.getBaseContext());
        if (connection.HasNetworkConnection()){
            if (!(etPassword.getText().length() > 0)){
                etPassword.setError("Empty Password!");
                etPassword.requestFocus(1);
            }
            if (!(etUsername.getText().length() > 0) || !etUsername.getText().toString().contains("@")){
                etUsername.setError("Invalid Email!");
                etUsername.requestFocus(0);
            }

            if (etUsername.getText().length() > 0 && etUsername.getText().toString().contains("@") &&
                    etPassword.getText().length() > 0) {
                username = etUsername.getText().toString();
                password = etPassword.getText().toString();
                new LoginUser().execute();
                //Toast.makeText(LoginActivity.this, username + " " + password, Toast.LENGTH_SHORT).show();
                if (isLoginSaving)
                    saveLogin();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
            builder.setMessage("You are not connected to the internet");
            builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }


    }

    private void saveLogin() {
        SharedPreferences preferences = getSharedPreferences("LOGIN_PREF", MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString("key_username", username);
        editor.putString("key_password", password);
        editor.putBoolean("key_loginStatus", true);

        editor.commit();
    }

    public class LoginUser extends AsyncTask<String, String, String>{

        private boolean flag;
        private ProgressDialog progressDialog;
        private JSONParser jsonParser;

        private static final String LOGIN_URL = "http://healthpal.comlu.com/login.php?platform=mobile";
        private static final String TAG = "TAG";
        private static final int SUCCESS = 0;
        private static final int NO_USER = 1;
        private static final int WRONG_PWD = 2;
        private static final int ERROR = 3;
        private static final int DATABASE_ERROR = 999;
        private boolean no_user = false;
        private boolean dbError = false;
        private boolean error = false;

        public LoginUser(){
            jsonParser = new JSONParser();

            progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Please wait while logging in");
            progressDialog.setIndeterminate(false);
            progressDialog.setCancelable(true);
        }

        @Override
        protected void onPreExecute() {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... params) {

            HashMap<String, String> args = new HashMap<>();
            args.put("email", username);
            args.put("pwd", password);

            JSONObject jsonObject = jsonParser.MakeHttpRequest(LOGIN_URL, "POST", args);

            try {
                int success = jsonObject.getInt(TAG);
                switch (success) {
                    case SUCCESS:
                        flag = true;
                        break;
                    case WRONG_PWD:
                        flag = false;
                        break;
                    case NO_USER:
                        no_user = true;
                        break;
                    case DATABASE_ERROR:
                        dbError = true;
                        break;
                    case ERROR:
                        error = true;
                        break;
                    default:
                        error = true;
                        break;
                }
            } catch (JSONException e) {
                Toast.makeText(getApplicationContext(), "ERROR: " + e.getMessage(),
                        Toast.LENGTH_SHORT).show();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();

            if (flag){
                Toast.makeText(getApplicationContext(), "You are now logged in!",
                        Toast.LENGTH_LONG).show();
                Intent mapIntent = new Intent(getApplicationContext(), MapsActivity.class);
                startActivity(mapIntent);

            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("Login Failed!");
                builder.setMessage("Please check your email and password and try again");
                builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                Dialog d = builder.create();
                d.show();
            }
        }
    }
}
