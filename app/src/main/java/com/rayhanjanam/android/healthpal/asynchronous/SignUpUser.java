package com.rayhanjanam.android.healthpal.asynchronous;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.rayhanjanam.android.healthpal.LoginActivity;
import com.rayhanjanam.android.healthpal.associate.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Rayhan on 06-Feb-16.
 */
public class SignUpUser extends AsyncTask<String, String, String> {
    private Context appContext;
    private Bundle bundle;
    private ProgressDialog progressDialog;
    private JSONParser jsonParser = new JSONParser();

    private static final String SIGNUP_URL = "http://healthpal.comlu.com/register.php?platform=mobile";
    private static final String TAG = "TAG";
    private static final int SUCCESS = 10;
    private static final int FAILED = 11;
    private static final int REGISTERED = 12;
    private static final int DATABASE_ERROR = 999;
    private boolean flag;
    private boolean preRegistered = false;
    private boolean dbError = false;
    private boolean unknown = false;

    public SignUpUser(Context context, Bundle b){
        this.bundle = b;
        this.appContext = context;
    }

    @Override
    protected void onPreExecute() {
        progressDialog = new ProgressDialog(appContext);
        progressDialog.setMessage("Please wait...\nYour account is being created");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... params) {
        HashMap<String, String> parameters = new HashMap<>();

        String donor = (bundle.getBoolean("donor")) ? "1" : "0";
        String pub = (bundle.getBoolean("donor")) ? "1" : "0";

        parameters.put("sname", bundle.getString("name"));
        parameters.put("semail", bundle.getString("email"));
        parameters.put("spwd", bundle.getString("password"));
        parameters.put("phn", bundle.getString("phn"));
        parameters.put("blood", bundle.getString("blood"));
        parameters.put("donor", donor);
        parameters.put("public", pub);

        JSONObject jsonObject = jsonParser.MakeHttpRequest(SIGNUP_URL, "POST", parameters);

        try {
            int success = jsonObject.getInt(TAG);

            switch (success){
                case SUCCESS:
                    flag = true;
                    break;
                case FAILED:
                    flag = false;
                    break;
                case REGISTERED:
                    preRegistered = true;
                    break;
                case DATABASE_ERROR:
                    dbError = true;
                    break;
                default:
                    unknown = true;
                    break;
            }

        } catch (JSONException e) {
            Toast.makeText(this.appContext, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    @Override
    protected void onPostExecute(String s) {
        progressDialog.dismiss();

        if (flag){
            Toast.makeText(this.appContext, "You are now registered\n Log In Now", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(this.appContext, LoginActivity.class);
            appContext.startActivity(intent);
        } else {
            String message = "";
            if (preRegistered){
                message = "You are already registered!";
            } else if (dbError){
                message = "Server Error! Please try again later";
            } else if (unknown){
                message = "Unknown Error!";
            } else if (!flag){
                message = "Can't register you now!";
            } else
                message = "";


            AlertDialog.Builder dBuilder = new AlertDialog.Builder(this.appContext);

            dBuilder.setTitle("Sign Up failed!");
            dBuilder.setMessage(message);
            dBuilder.setNeutralButton("Ok",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog,
                                            int which) {
                            dialog.cancel();
                        }
                    });
            dBuilder.setCancelable(true);

            AlertDialog signupAlert = dBuilder.create();
            signupAlert.show();

        }
        super.onPostExecute(s);
    }
}