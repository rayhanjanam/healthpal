package com.rayhanjanam.android.healthpal.gplace;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Rayhan on 16-Feb-16.
 */
public class GooglePlace {
    private String id;
    private String icon;
    private String name;
    private String vicinity;
    private Double latitude;
    private Double longitude;

    public GooglePlace() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVicinity() {
        return vicinity;
    }

    public void setVicinity(String vicinity) {
        this.vicinity = vicinity;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public static GooglePlace JSONtoPlace(JSONObject jsonObject){
        try {
            GooglePlace googlePlace = new GooglePlace();
            JSONObject geometry = (JSONObject) jsonObject.get("geometry");
            JSONObject location = (JSONObject) geometry.get("location");

            googlePlace.setLatitude((Double) location.get("lat"));
            googlePlace.setLongitude((Double) location.get("lng"));
            googlePlace.setIcon(jsonObject.getString("icon"));
            googlePlace.setName(jsonObject.getString("name"));
            googlePlace.setVicinity(jsonObject.getString("vicinity"));
            googlePlace.setId(jsonObject.getString("id"));
            return googlePlace;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }
}
