package com.rayhanjanam.android.healthpal;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.rayhanjanam.android.healthpal.associate.NetworkConnection;
import com.rayhanjanam.android.healthpal.asynchronous.SignUpUser;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;


public class SignupActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText etName, etEmail, etPassword, etRePassword;
    private Spinner spBloodGroup;
    private CheckBox cbDonor, cbPublicPhone;
    private Button btnSignUp;

    private boolean isDonor, isPublicPhone;
    private String name, email, password, rePassword, bloodGroup, phnNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        init();
    }

    private void init() {
        etName = (EditText) findViewById(R.id.signup_etName);
        etEmail = (EditText) findViewById(R.id.signup_etEmail);
        etPassword = (EditText) findViewById(R.id.signup_etPasswd);
        etRePassword = (EditText) findViewById(R.id.signup_etPasswd2);
        spBloodGroup = (Spinner) findViewById(R.id.signup_spBloodgrp);
        cbDonor = (CheckBox) findViewById(R.id.signup_cbDonor);
        cbPublicPhone = (CheckBox) findViewById(R.id.signup_cbPublicPhn);
        btnSignUp = (Button) findViewById(R.id.signup_btnSignup);

        btnSignUp.setOnClickListener(this);

        cbDonor.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    isDonor = true;
                else
                    isDonor = false;
            }
        });

        cbPublicPhone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked)
                    isPublicPhone = true;
                else
                    isPublicPhone = false;
            }
        });
    }

    private boolean getDataFromFields() {
        boolean success = true;
        name = etName.getText().toString();
        if (name.length() <= 0) {
            etName.setError("Name required");
            success = false;
        }

        email = etEmail.getText().toString();
        if (email.length() <= 0 && email.contains("@")) {
            etEmail.setError("Invalid Email");
            success = false;
        }

        password = etPassword.getText().toString();
        if (password.length() < 4) {
            etPassword.setError("Minimum 4 character");
            success = false;
        }

        rePassword = etRePassword.getText().toString();
        if (!password.equals(rePassword)) {
            etRePassword.setError("Password mismatch");
            success = false;
        }
        bloodGroup = spBloodGroup.getSelectedItem().toString();
        isDonor = cbDonor.isChecked();
        isPublicPhone = cbPublicPhone.isChecked();
        //phnNumber = getPhoneNumber();
        phnNumber = "0123456789";
        return success;
    }

    EditText etNumber;
    Button btnDone, btnCancel;
    String number = "";

    protected String getPhoneNumber() {
        if (isDonor) {
            TelephonyManager telephonyManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
            number = telephonyManager.getLine1Number();

            if (number.isEmpty()) {
                final Dialog dialog = new Dialog(SignupActivity.this);
                dialog.setCanceledOnTouchOutside(false);
                dialog.setCancelable(false);
                dialog.setTitle(R.string.app_name);
                dialog.setContentView(R.layout.dialog_signup_phonenumber);


                etNumber = (EditText) dialog.findViewById(R.id.signup_dialog_etNumber);
                btnDone = (Button) dialog.findViewById(R.id.signup_dialog_btnDone);
                btnCancel = (Button) dialog.findViewById(R.id.signup_dialog_btnCancel);

                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (etNumber.getText().length() > 0) {
                            number = etNumber.getText().toString();
                            dialog.dismiss();
                        }
                    }
                });

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        isPublicPhone = false;
                        dialog.dismiss();
                    }
                });

                dialog.show();
            }
            return number;
        }
        else
            return null;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.signup_btnSignup:
               if (getDataFromFields()){
                   execSignUp();
               }
        }
    }

    private void execSignUp() {
        NetworkConnection connection = new NetworkConnection(this.getBaseContext());
        if (connection.HasNetworkConnection())
        {
            Bundle bundle = new Bundle();
            bundle.putString("name", name);
            bundle.putString("email", email);
            bundle.putString("password", password);
            bundle.putString("blood", bloodGroup);
            bundle.putString("phn", phnNumber);
            bundle.putBoolean("donor", isDonor);
            bundle.putBoolean("public", isPublicPhone);

            new SignUpUser(SignupActivity.this, bundle).execute();
        }
        else
            Toast.makeText(SignupActivity.this, "You are not connected to the Internet", Toast.LENGTH_SHORT).show();
    }

}
